package com.example.screamitus_android;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewAssertion;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;
import android.widget.EditText;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.*;
import android.support.test.rule.ActivityTestRule;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    Infection infection;


    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.screamitus_android", appContext.getPackageName());
    }

    //R1: The number of days must be > 0
    @Test
    public void testNumberOfDays(){
        int noOfDays = 0;
        int expectedOutput = -1;
        infection = new Infection();
        assertEquals(expectedOutput,infection.calculateTotalInfected(noOfDays));

    }

    //R2: Virus infects at rate of infection is 5 instructors per day
   @Test
    public void test5InfectedInstructorsPerDay(){
        int noOfDays = 3;
        int expectedOutput = 15;
        infection = new Infection();
        assertEquals(expectedOutput,infection.calculateTotalInfected(noOfDays));

   }

   //R3: After 7th day rate of infection is 8 instructors per day
    @Test
    public void test8InfectedInstructorsPerDay(){
        int noOfDays = 9;
        int expectedOutput = 51;
        infection = new Infection();
        assertEquals(expectedOutput,infection.calculateTotalInfected(noOfDays));

    }

    //R4: Pritesh, Mohammed and Albert at College ( Infection drops to zero at Even days)
    @Test
    public void testInfectedInstructorsAtEvenDays(){
        int noOfDays = 10;
        int expectedOutput = 0;
        infection = new Infection();
        assertEquals(expectedOutput,infection.calculateTotalInfected(noOfDays));

    }


    @Rule
    public ActivityTestRule activityRule =
            new ActivityTestRule<>(MainActivity.class);

    //TC1: When the app loads, the text box and button are visible, but results label is hidden
    @Test
    public void testsElementsVisibility(){
        //1. Fetch the IDs oif required elements & check if exepected Output is equal to actual
         assertEquals(true, onView(withId(R.id.daysTextBox)).check(matches(isDisplayed())));
         assertEquals(true,onView(withText("Calculate")));
         assertEquals(true,onView(withId(R.id.resultsLabel)).check(matches(isDisplayed())));

    }

    //TC2: When users enters no of days and presses the button, correct value is displayed

    @Test
    public void testsCorrectResultDisplayed(){
      //1. Fetch the Elements and perform required actions
        onView(withId(R.id.daysTextBox)).perform(typeText("8"));
        onView(withText("Calculate")).perform(click());

        //2. Get the result
        String text = onView(withId(R.id.resultsLabel)).toString();
        assertEquals("43",text);
    }


}
